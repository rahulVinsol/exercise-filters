//
//  BasePhotoViewController.swift
//  ExerciseFilter
//
//  Created by Rahul Rawat on 30/06/21.
//

import UIKit
import PhotosUI
import MobileCoreServices

class BasePhotoViewController: UIViewController {
    
    func showImagePickingSheet() {
        let actionSheet = UIAlertController(title: "Choose Image from", message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad ? .alert : .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { [weak self] action in
            self?.pickImageTapped()
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] action  in
            self?.clickImageTapped()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func pickImageTapped() {
        if #available(iOS 14, *) {
            var configuration = PHPickerConfiguration()
            configuration.filter = .images
            configuration.selectionLimit = 1
            configuration.preferredAssetRepresentationMode = .current
            
            let picker = PHPickerViewController(configuration: configuration)
            picker.delegate = self
            present(picker, animated: true, completion: nil)
        } else {
            checkAndShowPicker(sourceType: .photoLibrary)
        }
    }
    
    private func clickImageTapped() {
        checkAndShowPicker(sourceType: .camera)
    }
    
    private func checkImage(image: UIImage?) {
        DispatchQueue.main.async { [weak self] in
            if image == nil {
                self?.showErrorAlertVC(title: "Error", message: "Unable to pick Images")
            } else {
                self?.pickedImage(image: image!)
            }
        }
    }
    
    func pickedImage(image: UIImage) {
        
    }
}

extension BasePhotoViewController: PHPickerViewControllerDelegate {
    
    @available(iOS 14, *)
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        dismiss(animated: true, completion: nil)
        
        if let result = results.first {
            let itemProvider = result.itemProvider
            if itemProvider.canLoadObject(ofClass: UIImage.self) {
                itemProvider.loadObject(ofClass: UIImage.self) { [weak self] image, error in
                    self?.checkImage(image: image as? UIImage)
                }
            } else {
                checkImage(image: nil)
            }
        }
    }
}

extension BasePhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func checkAndShowPicker(sourceType: UIImagePickerController.SourceType) {
        if !UIImagePickerController.isSourceTypeAvailable(sourceType) {
            var message: String?
            switch sourceType {
            case .camera:
                message = "Can't Find Camera"
            case .photoLibrary:
                message = "Can't Find Photo Library"
            case .savedPhotosAlbum:
                message = "Can't Find Saved Albums"
            default:
                message = nil
            }
            
            let alert = UIAlertController(title: "Not Supported", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        if sourceType == .camera && !checkCameraPermission() {
            return
        }
        
        showPicker(sourceType: sourceType)
    }
    
    func checkCameraPermission() -> Bool {
        switch AVCaptureDevice.authorizationStatus(for: .video){
        case .authorized:
            return true
        case .denied:
            showPermissionDeniedVC()
            return false
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] success in
                DispatchQueue.main.async {
                    if success {
                        self?.showPicker(sourceType: .camera)
                    } else {
                        self?.showPermissionDeniedVC()
                    }
                }
            })
            return false
        case .restricted:
            return false
        default:
            return false
        }
    }
    
    func showPermissionDeniedVC() {
        let alert = UIAlertController(title: "Permission Denied", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func showPicker(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
        imagePickerController.sourceType = sourceType
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        switch info[UIImagePickerController.InfoKey.mediaType] as? String {
        
        case String(kUTTypeImage):
            let pickedImage = (info[UIImagePickerController.InfoKey.editedImage] ?? info[UIImagePickerController.InfoKey.originalImage]) as? UIImage
            if let image = pickedImage {
                checkImage(image: image)
                return
            }
        default:
            break
        }
        checkImage(image: nil)
    }
}

extension UIViewController {
    func showErrorAlertVC(title: String, message: String? = nil, error: Error? = nil) {
        let alert = UIAlertController(title: title, message: message ?? error?.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
