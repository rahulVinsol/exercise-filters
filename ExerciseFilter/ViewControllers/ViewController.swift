//
//  ViewController.swift
//  ExerciseFilter
//
//  Created by Rahul Rawat on 30/06/21.
//

import UIKit
import CoreImage

class ViewController: BasePhotoViewController {

    @IBOutlet weak var pickImageButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    private var originalImage: UIImage?
    
    private lazy var filters: [Filter] = {
        var filterList: [Filter] = []
    
        filterList.append(Filter(displayName: "Vivid", isClearFilter: false, ciFilterName: "CIVibrance", ciFilterParams: [kCIInputAmountKey: 1.0]))
        
        filterList.append(Filter(displayName: "Pop Art", isClearFilter: false, ciFilterName: "CIColorPosterize", ciFilterParams: ["inputLevels": 8.0]))
        
        filterList.append(Filter(displayName: "Antique", isClearFilter: false, ciFilterName: "CIPhotoEffectChrome", ciFilterParams: [:]))
        
        filterList.append(Filter(displayName: "Vintage Distorted", isClearFilter: false, ciFilterName: "CIPhotoEffectInstant", ciFilterParams: [:]))
        
        filterList.append(Filter(displayName: "Vintage Cool", isClearFilter: false, ciFilterName: "CIPhotoEffectProcess", ciFilterParams: [:]))
        
        filterList.append(Filter(displayName: "Bloom", isClearFilter: false, ciFilterName: "CIBloom", ciFilterParams: [kCIInputRadiusKey: 12.0, kCIInputIntensityKey: 0.8]))
        
        filterList.append(Filter(displayName: "Comic", isClearFilter: false, ciFilterName: "CIComicEffect", ciFilterParams: [:]))
        
        filterList.append(Filter(displayName: "Gloom", isClearFilter: false, ciFilterName: "CIGloom", ciFilterParams: [kCIInputRadiusKey: 12.0, kCIInputIntensityKey: 0.8]))
        
        filterList.append(Filter(displayName: "Point", isClearFilter: false, ciFilterName: "CIPointillize", ciFilterParams: [kCIInputRadiusKey: 12.0]))
        
        filterList.append(Filter(displayName: "Clear Filters",isClearFilter: true, ciFilterName: "Clear Frame", ciFilterParams: [:]))
        
        return filterList
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView.hidesWhenStopped = true
        
        pickImageButton.layer.cornerRadius = 15
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    @IBAction func pickImageTapped(_ sender: UIButton) {
        showImagePickingSheet()
    }
    
    override func pickedImage(image: UIImage) {
        originalImage = image
        imageView.image = image
    }
    
    private func applyFilterTo(image: UIImage, filter: Filter) -> UIImage? {
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
        
        guard let mtlDevice = MTLCreateSystemDefaultDevice(), let ciImage = CIImage(image: image),
              let ciFilter = CIFilter(name: filter.ciFilterName) else {
            return nil
        }
        let context = CIContext(mtlDevice: mtlDevice)
        
        ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
        
        for (key, value) in filter.ciFilterParams {
            ciFilter.setValue(value, forKey: key)
        }
        
        var filteredImage: UIImage?
        
        if let output = ciFilter.outputImage {
            if let cgImageResult = context.createCGImage(output, from: output.extent) {
                filteredImage = UIImage(cgImage: cgImageResult)
            }
        }
        
        activityIndicatorView.stopAnimating()
        
        return filteredImage
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.IDENTIFIER, for: indexPath) as! CollectionViewCell
        
        let filter = filters[indexPath.row]
        cell.setupView(filter: filter)
        
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let image = imageView.image else { return }
        
        let filter = filters[indexPath.row]
        if !filter.isClearFilter, let filteredImage = applyFilterTo(image: image, filter: filters[indexPath.row]) {
            imageView.image = filteredImage
        } else {
            imageView.image = originalImage
        }
    }
}
