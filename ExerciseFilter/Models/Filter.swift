//
//  Filter.swift
//  ExerciseFilter
//
//  Created by Rahul Rawat on 30/06/21.
//

import Foundation

struct Filter {
    let displayName: String
    let isClearFilter: Bool
    let ciFilterName: String
    let ciFilterParams: [String : Any]
}
