//
//  CollectionViewCell.swift
//  ExerciseFilter
//
//  Created by Rahul Rawat on 30/06/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    static let IDENTIFIER = "cell"
    
    @IBOutlet weak var label: UILabel!
    
    func setupView(filter: Filter) {
        label.text = filter.displayName
    }
}
